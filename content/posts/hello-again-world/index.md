---
title: "Hello Again World"
author: "Daniël Franke"
authorTwitter: ainmosni
date: 2022-01-10T18:21:57+01:00
keywords:
- personal
categories:
- Personal
tags:
- Hugo
- Blog
showFullContent: false
readingTime: false
description: Just some musings on returning to blogging.
---

After not having blogged in like... forever, I thought it was time to set up a blog again.
In lieu of blogging, I've been pretty active on everyone's favourite [hellsite](https://twitter.com/ainmosni)
while slowly getting more active in the [fediverse](https://social.tchncs.de/web/accounts/107275063467307743).

It's been a pretty wild couple of years, with huge changes both globally (you know the ones I'm talking about)
and personally (which I will write about in due time). This blog will be a place for me to rant, ramble, and
comment on those, and whatever else comes to mind. I tend to write my tweets pretty off the cuff, and my aim
is to have a more coherent, and better edited version of my thoughts collected here.

The blog itself is based on [hugo](https://gohugo.io) and using the [terminal](https://github.com/panr/hugo-theme-terminal)
theme. One of the reasons it took me so long to set up a blog again, is because every time I wanted,
I started shaving yaks in making my own blogging engine in whatever technology interested me at that time.
This time, I decided that I just wanted a venting space without all the yak shaving, and I've always enjoyed
working with hugo, so that's what I went with.

Anyway, that's all for now, as this post is mostly to not have an empty blog published.