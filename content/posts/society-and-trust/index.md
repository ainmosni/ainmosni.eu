---
title: You can't have society without trust
date: 2022-02-07T11:52:40+01:00
author: Daniël Franke 
authorTwitter: ainmosni #do not include @ 
tags:
- trust 
- blockchain 
- cryptocurrency 
- nft 
- web3 
- tech 
- society 
- scamtech 
keywords: 
- blockchain 
- technology 
- web3 
categories: 
- technology 
- web3 
- society
description: |
  One of the biggest buzzwords about blockchain and cryptocurrency
  is that it's a trustless setup. But is that really true? And if it is, is it
  really something we should desire?  
showFullContent: false
readingTime: false
---

As the discourse about web3 and all the related technology becomes more intense,
I'd like to talk about trust. Trust is a concept that the web3 proponents seem
to despise, as one their main selling points is that the blockchain is
is trustless. The way they talk about trust, makes trust sound like the biggest
weakness in our society. But do they have a point?  Would society really work
better if nobody trusted each other?  Is it even possible?

Let's examine the concept of trust. I could do the cliché thing, and quote some
dictionary's definition of trust, but that's been done to death. Trust is the
very cornerstone of any society, and I don't think it would be a very
controversial statement to say that without trust, the entirety of human
civilisation would have been impossible. From the moment we're born, we trust.
We trust our parents to take care of our needs when we're children. We trust our
friends to treat us with respect. We trust adults to share knowledge that we'll
need in our lives. This implicit trust we're born with extends into our adult
life, and how we interact with society. We trust restaurants to not poison us.
We trust doctors to make us healthier. We trust, we trust, we trust.

I would go so far to posit that trust is so ingrained in us as a species, that
we've gone blind to all the things we implicitly trust. This trust is so close
to our identity, that one of the worst feelings in our lives is when this trust
is betrayed. Having our trust betrayed damages us, it changes how we interact
with the world, and depending on the severity of the breach, it can make us
bitter, and can make it hard to form meaningful connections with people.

But even in those cases, the implicit trust that we have in the basics of our
society remain. We don't worry that the food at the supermarket will kill us, we
don't worry that the things we buy will not work. Of course, one might argue
that we only trust these things because of legal regulations of where we live,
which is correct for many, but that just changes where we put our trust.

So, in comes this group, pushing a specific technology, where a large contingent
claims that the problem with our systems is the trust part, citing a lot of
valid criticisms of our current finance system as justification for their
claims. They name all the failings of modern banks, government policy, and other
authorities in how our current finance system is managed, and then claim that
the cryptocurrency will solve all these failings, as the blockchain it's built
on does not require trust. 

Now I will not go into if cryptocurrency is truly trustless from a technical
point of view, that is the subject for its own article, nor will I describe
how the cryptocurrency advocates claim that a trustless architecture is
achieved, there are a lot of papers out there on that already. For this article,
let's assume that transactions on the chain are 100% trustless, and that there
is no way to alter data on the chain once it is written. These assumptions also
include that we're talking about a proof-of-work blockchain, and that its system
itself is bug free and secure. After all, the aim of this article is not to
criticise the technology itself, there will be more than enough time for that in
other articles. This is about trust, and to examine if this trustless,
ungoverned finance system is feasible.

Let's get the obvious out of the way, living without trust in anything would be
absolutely miserable. One would have to either grow/make/do everything
themselves, something absolutely impossible in the modern world, or one would
have to spend considerable time verifying if anybody that sells/gives you
anything has done their job properly, and isn't out to get you. I don't
think I have to go in-depth on how this would be a horrible life. Besides,
I don't think anyone is arguing for a world completely devoid of trust. And
if anybody reading this actually is advocating for that world, I would advise
you to seek help from a loved one, or if you don't have those because of the
aforementioned lack of trust, professional help.

To get back to talking about the finance system, and what trust means there.
We don't often notice it, but our finance system has *a lot* of structures for
consumer protection. Fraudulent transactions can be reversed, typos in
IBANs/credit card numbers are caught, shady transactions are blocked, and so on.
Of course, for all this to work, we put trust in our financial system, and that
trust is mostly justified, at least in most stable countries, and on a personal
scale. There is a lot to say about all the problems with our finance system on a
more macro scale, and a lot of people have, and have great points, but again,
that is not what I want to talk about here.

Now we'll look at a system as described earlier, where the finance system itself
is not trusted, where it is just an immutable public ledger of transactions, nothing
more, nothing less. What does that mean? To get right to the point, it means that
you move the point of trust to a different place, many many different places. Because
without the trust in the finance system, you have to trust everything around it.
You have to trust that the seller is legitimate, because there is no way to get your
money back if it turns out they aren't. You have to be sure the wallet you're sending
the money to is legitimate, because again, once sent, there is no way to undo the
transaction. If your "wallet" is stolen, or you are locked out of your wallet, all
the contents are just inaccessible because there is nobody that can help reclaim it.
This trustless setup means that any interaction with the finance system can't be undone,
so therefore, you need to put a lot more trust in everything around it. So, despite this
system claiming that there will be no need for trust anymore, in practice this system
requires any user to trust more parties, and to trust them a lot more.

This "trustless" financial system creates the perfect environment for [scammers and grifters](https://web3isgoinggreat.com/)
and one could argue, very convincingly I might add, that that is the entire point of
this system. Post anything to do with the crypto ecosystem on twitter, and there
will be multiple scam posts replying to you, and this system is one of the main reasons
why ransomware is as big and damaging as it is now.

In conclusion, by trying to get rid of trust, this system has created a culture
of scams, grifts, and fleeced marks. And the proponents want our entire society to
adopt this system, because the biggest problem, according to them, is that we need
to trust the system.

Except they're missing one key fact...

 You can't have a society without trust.
